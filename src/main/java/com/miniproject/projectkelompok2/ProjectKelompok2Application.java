package com.miniproject.projectkelompok2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectKelompok2Application {

	public static void main(String[] args) {
		SpringApplication.run(ProjectKelompok2Application.class, args);
	}

}
